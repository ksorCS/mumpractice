public class Madhav {
	public static void main(String[] args) {
		int[] arr ={2, 1, 1};
		System.out.println(isMadhavArray(arr));
	}

	public static int isMadhavArray(int[] a) {
		if (!SquareTest.isSquare(8*a.length + 1)) {
			return 0;
		}
		int sum = a[0];
		int factorCount = 1;
		for (int i = 1; i + factorCount < a.length; i+=factorCount) {
			if (arraySum(a, i, i + factorCount) != sum) {
				return 0;
			}
			factorCount++;
		}

		return 1;
	}

	public static int arraySum(int[] array, int startPos, int endPost) {
		int sum = 0;
		for (int i = startPos; i <= endPost; i++) {
			sum += array[i];
		}
		return sum;
	}
}
