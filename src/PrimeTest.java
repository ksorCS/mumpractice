
public class PrimeTest {
    
    
    public static void main(String[] args) {
            System.out.println(isPrime(3));
    }

    public static boolean isPrime(int number) {
        if (number < 2)
            return false;
        if (number % 2 == 0)
            return false;
        if(number == 3) return true;
        if(!(number % 6 ==1 || number %6 ==5)) return false;
        for (int i = 3; i <= Math.sqrt(number); i += 2) {
            if (number % i == 0)
                return false;
        }
        return true;
    }

}
