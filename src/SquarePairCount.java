public class SquarePairCount {
	public static void main(String[] args) {
		int[] arr = {11, 5, 4, 20, 16, 38, 14, -5, 26} ;
		System.out.println(countSquarePairs(arr));
	}

	public static int countSquarePairs(int[] a) {
		int count = 0;
		for (int i = 0; i < a.length; i++) {
			for (int j = i + 1; j < a.length; j++) {
				if (a[i] > 0 && a[j] > 0 && a[i] != a[j]) {
					if (isSquare(a[j] + a[i])) {
						count++;
					}
				}
			}
		}

		return count;
	}

	public static boolean isSquare(int number) {
		int sqrt = (int) Math.sqrt(number);
		return sqrt * sqrt == number;
	}
}
