public class SquareTest {
    public static void main(String[] args) {
        System.out.println(isSquare(16));
    }
    public static boolean isSquare(int number) {
        for (int i = 0; i * i <= number; i++) {
            if (i * i == number) {
                return true;
            }
        }
        return false;
    }
}
