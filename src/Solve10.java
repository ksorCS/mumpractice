import java.util.Arrays;

public class Solve10 {
	public static void main(String[] args) {
		System.out.println(Arrays.toString(solve10()));
	}

	static int[] solve10() {
		for (int i = 1; i <= 10; i++) {
			int right = productOfRange(i +1 , 10);
			for (int delta = 1 ; delta < 10 - i; delta++) {
				int left = 1 + productOfRange(i+1, i + delta);
				if (left == right) {
					return new int[] { i, delta + i };
				}
			}
		}
		return null;
	}

	public static int productOfRange(int start, int end) {
		if (start == end)
			return start;
		return start * productOfRange(start + 1, end);
	}
}
