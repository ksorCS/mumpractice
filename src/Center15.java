import java.util.Arrays;

public class Center15 {
	public static void main(String[] args) {
		int[] a = {1, 1, 1, 15, -1,-1, 1};
		System.out.println(isCenter15(a));
	}

	public static boolean isCenter15(int[] a) {
		int sum = 0;
		for (int i = 0; i < a.length; i++) {
			sum += a[i];
		}
		if (sum == 15)
			return true;
		if (a.length <= 2)
			return sum == 15;

		int[] croppedArr = Arrays.copyOfRange(a, 1, a.length - 1);
		return isCenter15(croppedArr);
	}
}
