import java.util.HashMap;
import java.util.Map;

public class StatonMesure {
	public static void main(String[] args) {
		int[] arr = { 1, 3, 1, 1, 3, 3, 2, 3, 3, 3, 4 };
		System.out.println(stantonMeasure(arr));
	}

	public static int stantonMeasure(int[] a) {
		Map<Integer, Integer> existenceMap = new HashMap<>();
		for (Integer number : a) {
			if (existenceMap.containsKey(number)) {
				int currentExistValue = existenceMap.get(number);
				existenceMap.put(number, currentExistValue + 1);
			} else {
				existenceMap.put(number, 1);
			}
		}
		int stantonValue = 0;
		for (int existValue : existenceMap.values()) {
			if (existValue > stantonValue) {
				stantonValue = existValue;
			}
		}
		return stantonValue;
	}
}
