public class Vesuvian {
    public static void main(String[] args) {
        int totalVs = 0;
        for(int i =0 ; i< 10000;i++){
            if(isVesuvian(i)){
                totalVs++;
            }
        }
        System.out.println(totalVs);
    }

    public static boolean isVesuvian(int number) {
        int rangeToCalculate = (int) Math.sqrt(number);
        int numberOfPair = 0;
        for (int i = 1; i <= rangeToCalculate; i++) {
            int firtNumber = getSquareOf(i);
            int remaining = number - firtNumber;

            if (isPerfectSquareNumber(remaining)) {
                numberOfPair++;
                if(firtNumber == remaining){
                    numberOfPair++;
                }
            }
        }
        return numberOfPair == 4;
    }

    public static int getSquareOf(int number) {
        return number * number;
    }

    public static boolean isPerfectSquareNumber(int number) {
        if(number ==0) return false;
        int sqrt = (int) Math.sqrt(number);
        return number == sqrt * sqrt;
    }

}
