import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PrimeFind {
	public static void main(String[] args) {
		System.out.println(primeCount(-10, 6));
	}

	public static int primeCount(int start, int end) {
		int count = 0;
		boolean[] isPrimeArr = findPrimesArrayTo(end);
		int startCount = 0;
		if(start > 0){
			startCount = start;
		}
		for (int i = startCount; i <= end; i++) {
			if (isPrimeArr[i]) {
				count++;
			}
		}
		return count;
	}

	public static boolean[] findPrimesArrayTo(int number) {
		int size = number + 1;
		boolean[] isPrimeArr = new boolean[size];
		Arrays.fill(isPrimeArr, true);
		if (size < 2) {
			Arrays.fill(isPrimeArr, false);
			return isPrimeArr;
		}

		isPrimeArr[0] = false;
		isPrimeArr[1] = false;
		for (int i = 2; i <= number; i++) {
			if (isPrimeArr[i]) {
				for (int j = 2; i * j <= number; j++) {
					isPrimeArr[i * j] = false;
				}
			}
		}
		return isPrimeArr;
	}

	public static List<Integer> findPrimeInRange(int end) {
		boolean[] isPrimeArr = findPrimesArrayTo(end);
		List<Integer> primes = new ArrayList<>();

		for (int i = 0; i <= end; i++) {
			if (isPrimeArr[i]) {
				primes.add(i);
			}
		}

		return primes;
	}
}
