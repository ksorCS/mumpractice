public class Inertial {
	public static void main(String[] args) {
		int[] arr = {2, 4, 6, 8, 10, 9} ;
		System.out.println(isInertial(arr));
	}

	public static int isInertial(int[] a) {
		boolean isContainOdd = false;
		int maxValue = a[0];
		int minOdd = Integer.MAX_VALUE - 1;
		for (int i = 0; i < a.length; i++) {
			if (a[i] % 2 != 0) {
				isContainOdd = true;
				if (a[i] < minOdd)
					minOdd = a[i];
			}
			if (a[i] >= maxValue)
				maxValue = a[i];
		}

		if (!isContainOdd || maxValue % 2 != 0) {
			return 0;
		}
		for (int i = 0; i < a.length; i++) {
			if (a[i] != maxValue && a[i] % 2 == 0) {
				if (a[i] >= minOdd) {
					return 0;
				}
			}
		}
		return 1;
	}
}
