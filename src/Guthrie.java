public class Guthrie {

	public static void main(String[] args) {
		System.out.println(guthrieIndex(421111111));

	}

	static boolean isGuthrieSequence(int[] a) {
		int arrayLength = a.length;
		if (arrayLength >= 2) {
			if (a[arrayLength - 1] != 1)
				return false;

			int nextExpected = a[0];
			for (int i = 0; i < arrayLength; i++) {
				if (a[i] != nextExpected)
					return false;
				if (a[i] % 2 == 0) {
					nextExpected = a[i] / 2;
				} else {
					nextExpected = a[i] * 3 + 1;
				}
			}
		} else {
			return false;
		}

		return true;
	}

	static int guthrieIndex(int n) {
		if (n == 0)
			return 0;
		int nextExpected = n;
		int iterateTime = 0;
		while (nextExpected != 1) {
			if (nextExpected % 2 == 0) {
				nextExpected = nextExpected / 2;
			} else {
				nextExpected = nextExpected * 3 + 1;
			}
			iterateTime += 1;
		}
		return iterateTime;
	}
}
