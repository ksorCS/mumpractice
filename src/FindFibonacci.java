public class FindFibonacci {
    public static void main(String[] args) {
        System.out.println(isFibonacci(100000));
    }

    public static int findNthFib(int n) {
        double phi = (Math.sqrt(5) + 1) / 2;
        Double result = (Math.pow(phi, n) - Math.pow(-phi, -n)) / Math.sqrt(5);
        return result.intValue();
    }

    public static int findFibonacciClosestTo(int number) {
        double phi = (Math.sqrt(5) + 1) / 2;
        Double result = (Math.log(number) + Math.log(5) / 2) / Math.log(phi);
        return result.intValue();
    }

    public static boolean isFibonacci(int number) {
        int gesselNumber = 5*number*number;
        //Calculate by Gessel Formula
        return isPerfectSquareNumber(gesselNumber-4)||isPerfectSquareNumber(gesselNumber+4);
    }

    public static boolean isPerfectSquareNumber(int number) {
        int sqrt = (int) Math.sqrt(number);
        return number == sqrt*sqrt;
    }
}
