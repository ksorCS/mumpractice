public class OneBalance {
    public static void main(String[] args) {
        int[] arr = {};
        System.out.println(isOneBalanced(arr));
    }
    
    public static boolean isOneBalanced(int[] arr) {
        int length = arr.length;
        int noOf1 = 0;
        int startOfNone1 = 0;
        int endOfNone1 = 0;
        int noOfNone1 = 0;
        if (length == 0)
            return true;

        for (int i = 0; i < length; i++) {
            if (arr[i] != 1) {
                startOfNone1 = i;
                break;
            }
        }
        for (int i = 0; i < length; i++) {
            if (arr[i] != 1) {
                endOfNone1 = i;
                noOfNone1++;
            } else {
                noOf1++;
            }
        }

        for (int i = startOfNone1; i < endOfNone1; i++) {
            if (arr[i] == 1) {
                return false;
            }
        }
        return noOf1 == noOfNone1;
    }
}
