public class TriangularNumber {
    public static void main(String[] args) {
        System.out.println(isTriangular(666));
    }

    public static boolean isTriangular(int number) {
        return isPerfectSquareNumber(1 + 8 * number);
    }

    public static boolean isPerfectSquareNumber(int number) {
        int sqrt = (int) Math.sqrt(number);
        return number == sqrt * sqrt;
    }
}
