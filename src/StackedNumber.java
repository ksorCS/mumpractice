
public class StackedNumber {
	public static void main(String[] args) {
		System.out.println(isStacked(45));
	}

	// By Quadratic formula, n is Stacked if and only 1 + 8*n is square
	public static boolean isStacked(int n) {
		return SquareTest.isSquare(1 + 8 * n);
	}
}
