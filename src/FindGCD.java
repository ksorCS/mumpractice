public class FindGCD {
	public static void main(String[] args) {
		System.out.println(findGCD(36, 36));;
	}

	public static int findGCD(int a, int b) {
		if (a == 0 || b == 0) {
			return a + b;
		}
		if (a >= b) {
			return findGCD(b, a % b);
		} else {
			return findGCD(a, b % a);
		}
	}
}
