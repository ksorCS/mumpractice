import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ClusterCompression {
	public static void main(String[] args) {
		int[] arr = new int[] { 1, 1, 1, 1, 3, 3, 3, 3, 4, 4, 1, 1, 2 };
		System.out.println(Arrays.toString(clusterCompression(arr)));
	}

	public static int[] clusterCompression(int[] a) {
		if (a.length == 0) {
			return new int[] {};
		}
		int clusterValue = a[0];
		int compressionValue = 1;
		List<Integer> compressionList = new ArrayList<>();
		for (int i = 1; i < a.length; i++) {
			if (a[i] == clusterValue) {
				compressionValue++;
			} else {
				compressionList.add(compressionValue);
				clusterValue = a[i];
				compressionValue = 1;
			}
		}
		compressionList.add(compressionValue);

		int[] compressionArray = new int[compressionList.size()];
		for (int i = 0; i < compressionList.size(); i++) {
			compressionArray[i] = compressionList.get(i);
		}
		return compressionArray;

	}
}
