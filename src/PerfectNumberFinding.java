import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PerfectNumberFinding {
    public static void main(String[] args) {
        List<Integer> perfectNumber = findPerfectNumber();
        System.out.println(Arrays.toString(perfectNumber.toArray()));
    }
 
    
	private static List<Integer> findPerfectNumber() {
		List<Integer> firstPrimes = PrimeFind.findPrimeInRange(15);
		List<Integer> perfectNumbers = new ArrayList<>();
		for (int prime : firstPrimes) {
			int closestPowerTwo = (int) Math.pow(2, prime) - 1;
			if (PrimeTest.isPrime(closestPowerTwo)) {
				Integer perfectNumber = (int) Math.pow(2, prime - 1) * closestPowerTwo;
				perfectNumbers.add(perfectNumber);
			}
		}
		return perfectNumbers;
	}
    
	// Test for case number more than 31 bit
    public static List<BigInteger> findPerfectNumber2() {
        List<Integer> firstPrimes = PrimeFind.findPrimeInRange(20);
        List<BigInteger> perfectNumbers = new ArrayList<>();
        for (int prime : firstPrimes) {
            int closestPowerTwo = (int) Math.pow(2, prime) - 1;
            if (PrimeTest.isPrime(closestPowerTwo)) {
                BigInteger perfectNumber = BigInteger.valueOf((long)Math.pow(2, prime - 1))
                        .multiply(BigInteger.valueOf((long) closestPowerTwo));
                perfectNumbers.add(perfectNumber);
            }
        }
        return perfectNumbers;
    }
    
}
