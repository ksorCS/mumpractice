
public class SmallestMultiple {
	public static void main(String[] args) {
		System.out.println(smallest(4));
	}

	public static int smallest(int n) {
		int smallest = 1;
		while (true) {
			smallest++;
			int tmp = smallest;
			for (int i = 1; i <= n; i++) {
				if (!isNumberContain2(smallest * i)) {
					break;
				}
				tmp = smallest * i;
			}
			if (tmp == (smallest * n)) {
				return smallest;
			}

		}

	}

	private static boolean isNumberContain2(int number) {
		if (number < 10)
			return number == 2;
		if (number % 10 == 2)
			return true;
		return isNumberContain2(number / 10);
	}
}
